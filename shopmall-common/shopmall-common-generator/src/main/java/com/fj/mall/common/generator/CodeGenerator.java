package com.fj.mall.common.generator;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: fengjun
 * @Copyright: 合肥
 * @Description:
 * @Date: 2020/7/8 上午 09:01
 */
public class CodeGenerator {
    /* -------------------------------------- 这里以下进行配置-------------------------------------- */
    //数据库配置
    private static final String DATABASE_NAME = "mall";
    private static final String DATABASE_URL = "192.168.31.64:3306";
    private static final String DATABASE_USERNAME = "root";
    private static final String DATABASE_PASSWORD = "root";
    // 模块名称
    private static final String MODULE_KEY = "goods";


    //需要生成的表
    private static final String[] GENERATOR_TABLES = {
            "pms_goods"
    };

    /* -------------------------------------- 这里以上进行配置-------------------------------------- */






    private static final String MODULE_PATH = String.format("com.fj.mall.%s", MODULE_KEY);
    // mapper xml 输出路径
    private static final String XML_PATH = String.format("/src/main/java/com/fj/mall/%s/biz/dao/", MODULE_KEY);
    //模块路径
    private static final String PARENT_MODULE_NAME = "shopmall-common/shopmall-common-generator";
    /**
     * 固定字段,生成器在第二级目录时此字段务必设置为空
     */
    private static final String MODULE_NAME = "";
    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir") + "/" + PARENT_MODULE_NAME + "/" + MODULE_NAME;
        gc.setOutputDir(projectPath + "/src/main/java");
        //  设置用户名
        gc.setAuthor("fj");
        gc.setOpen(true);
        // service 命名方式
        gc.setServiceName("%sService");
        // service impl 命名方式
        gc.setServiceImplName("%sServiceImpl");
        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
        gc.setFileOverride(true);
        gc.setActiveRecord(true);
        // XML 二级缓存
        gc.setEnableCache(false);
        // XML ResultMap
        gc.setBaseResultMap(true);
        // XML columList
        gc.setBaseColumnList(false);
        mpg.setGlobalConfig(gc);

        //  数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(String.format("jdbc:mysql://%s/%s?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=Asia/Shanghai", DATABASE_URL, DATABASE_NAME));
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername(DATABASE_USERNAME);
        dsc.setPassword(DATABASE_PASSWORD);
        mpg.setDataSource(dsc);

        //  包配置
        PackageConfig pc = new PackageConfig();
        pc.setModuleName("");
        pc.setParent(MODULE_PATH);
        pc.setEntity("api.pojo.entity");
        pc.setMapper("biz.dao");
        pc.setService("api.service");
        pc.setServiceImpl("biz.service.impl");
        pc.setController("biz.controller");

        mpg.setPackageInfo(pc);

        // 自定义需要填充的字段
        // List<TableFill> tableFillList = new ArrayList<>();
        //如 每张表都有一个创建时间、修改时间
        //而且这基本上就是通用的了，新增时，创建时间和修改时间同时修改
        //修改时，修改时间会修改，
        //虽然像Mysql数据库有自动更新几只，但像ORACLE的数据库就没有了，
        //使用公共字段填充功能，就可以实现，自动按场景更新了。
        //如下是配置
        //TableFill createField = new TableFill("gmt_create", FieldFill.INSERT);
        //TableFill modifiedField = new TableFill("gmt_modified", FieldFill.INSERT_UPDATE);
        //tableFillList.add(createField);
        //tableFillList.add(modifiedField);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
//
        // 如果模板引擎是 freemarker
//		String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        String templatePath = "/templates/mapper.xml.vm";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + XML_PATH + "/mapper/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });

        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();


        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
//		strategy.setSuperEntityClass("com.gao.mybatisplusstudy.entity");
        strategy.setEntityLombokModel(true);
        strategy.setEntityBuilderModel(true);
        strategy.setRestControllerStyle(true);
        // 公共父类
//		strategy.setSuperControllerClass("com.gao.mybatisplusstudy.controller");
        // 写于父类中的公共字段
//		strategy.setSuperEntityColumns("id");
        //这里写表名
        strategy.setInclude(GENERATOR_TABLES);
        strategy.setControllerMappingHyphenStyle(true);
        strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);
//		mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();

    }
}
