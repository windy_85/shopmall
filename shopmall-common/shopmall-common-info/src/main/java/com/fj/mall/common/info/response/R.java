package com.fj.mall.common.info.response;

import com.fj.mall.common.info.constants.GlobalOperaConstant;
import lombok.Data;

/**
 * @Author: fengjun
 * @Copyright: 合肥
 * @Description:
 * @Date: 2020/8/6 下午 03:31
 */
@Data
public class R<T> {
    private int code;
    private String message;
    private T data;

    public static <T> R<T> ok(){
        return setResult(GlobalOperaConstant.SUCCESS, "success", null);
    }

    public static <T> R<T> ok(T data){
        return setResult(GlobalOperaConstant.SUCCESS, "success", data);
    }

    public static <T> R<T> ok(T data, String message){
        return setResult(GlobalOperaConstant.SUCCESS, message, data);
    }

    public static <T> R<T> fail(){
        return setResult(GlobalOperaConstant.FAIL, "failed", null);
    }

    public static <T> R<T> fail(T data){
        return setResult(GlobalOperaConstant.FAIL, "failed", data);
    }

    public static <T> R<T> fail(T data, String message){
        return setResult(GlobalOperaConstant.FAIL, message, data);
    }

    public static <T> R<T> setResult(int code, String message, T data){
        R<T> response = new R<T>();
        response.setCode(code);
        response.setMessage(message);
        response.setData(data);
        return response;
    }
}
