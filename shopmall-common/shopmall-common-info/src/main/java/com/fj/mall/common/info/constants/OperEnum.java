package com.fj.mall.common.info.constants;

/**
 * @Author: fengjun
 * @Copyright: 合肥
 * @Description:
 * @Date: 2020/8/6 下午 04:29
 */

public enum OperEnum {

    BASE_SYS_EX(3000, "System Error!");

    private final Integer code;
    private final String error;

    OperEnum(Integer code, String error){
        this.code = code;
        this.error = error;
    }

}
