package com.fj.mall.common.info.constants;

/**
 * @Author: fengjun
 * @Copyright: 合肥
 * @Description:
 * @Date: 2020/8/6 下午 04:09
 */
public class GlobalOperaConstant {

    public static final Integer SUCCESS = 1000;

    public static final Integer FAIL = 2000;

}
