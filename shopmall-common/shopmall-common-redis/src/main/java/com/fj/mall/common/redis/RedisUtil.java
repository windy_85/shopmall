package com.fj.mall.common.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import sun.util.resources.cldr.gv.LocaleNames_gv;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @Author: fengjun
 * @Copyright: 合肥
 * @Description:
 * @Date: 2020/7/7 上午 09:23
 */
@Component
public class RedisUtil {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 设置key的过期时间
     * @param key
     * @param expire
     */
    public void expire(String key, long expire){
        stringRedisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }


    /**
     * 删除key
     * @param key
     */
    public void delete(String key){
        stringRedisTemplate.delete(key);
    }

    //---------------------------------------------String------------------------------------------------------------/
    /**
     * 设置对象
     * @param key
     * @param value
     */
    public void set(String key, String value){
        stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * 设置对象，超时expire
     * @param key
     * @param value
     * @param expire
     */
    public void set(String key, String value, Long expire){
        stringRedisTemplate.opsForValue().set(key, value);
        if(expire > 0){
            stringRedisTemplate.expire(key, expire, TimeUnit.SECONDS);
        }
    }


    /**
     * 获取value值
     * @param key
     * @return
     */
    public String get(String key){
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 值自增
     * @param key
     * @param delta
     */
    public void increment(String key, long delta){
        stringRedisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 值递减
     * @param key
     * @param delta
     */
    public void decrement(String key, long delta){
        stringRedisTemplate.opsForValue().decrement(key, delta);
    }
    //---------------------------------------------Map---------------------------------------------------------------/

    /**
     * 向hash表中存入一条记录
     * @param key
     * @param hashKey
     * @param hashValue
     */
    public void hashSet(String key, String hashKey, String hashValue){
        stringRedisTemplate.opsForHash().put(key, hashKey, hashValue);
    }

    /**
     * 向hash表中存入map对象
     * @param key
     * @param map
     */
    public void hashMapSet(String key, Map<String, Object> map){
        stringRedisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 根据hash key获取一条记录
     * @param key
     * @param hashKey
     * @return
     */
    public String hashGet(String key, String hashKey){
        return stringRedisTemplate.<String, String>opsForHash().get(key, hashKey);
    }

    /**
     * 获取整个map对象
     * @param key
     * @return
     */
    public Map<String, String> hashMapGet(String key){
        return stringRedisTemplate.<String, String>opsForHash().entries(key);
    }

    /**
     * 删除hash表中多条记录
     * @param key
     * @param item
     */
    public void hashDelete(String key, Object... item){
        stringRedisTemplate.opsForHash().delete(key, item);
    }

    /**
     * hash递增，返回递增后的值
     * @param key
     * @param hashKey
     * @param delta
     * @return
     */
    public long hashIncr(String key, String hashKey, long delta){
        return stringRedisTemplate.opsForHash().increment(key, hashKey, delta);
    }

    /**
     * hash递减
     * @param key
     * @param hashKey
     * @param delta
     * @return
     */
    public long hashDecr(String key, String hashKey, long delta){
        return stringRedisTemplate.opsForHash().increment(key, hashKey, -delta);
    }

    //------------------------------------------------------set------------------------------------------------------/

    /**
     * 存入set数据
     * @param key
     * @param values
     */
    public void sSet(String key, String...values){
        stringRedisTemplate.opsForSet().add(key,values);
    }

    /**
     * 移除set中的元素
     * @param key
     * @param values
     */
    public void sDelete(String key, String...values){
        stringRedisTemplate.opsForSet().remove(key, values);
    }

    /**
     * 根据key获取set集合
     * @param key
     * @return
     */
    public Set<String> sGet(String key){
        return  stringRedisTemplate.opsForSet().members(key);
    }

    /**
     * 获取set集合的size
     * @param key
     * @return
     */
    public long sSize(String key){
        return stringRedisTemplate.opsForSet().size(key);
    }
    /**
     * 查询set中是否存在该元素
     * @param key
     * @param member
     * @return
     */
    public boolean sHasKey(String key, String member){
        return stringRedisTemplate.opsForSet().isMember(key, member);
    }

    //------------------------------------------------------list------------------------------------------------------/
    /**
     * list中存入一个元素
     * @param key
     * @param value
     */
    public void leftPush(String key, String value){
        stringRedisTemplate.opsForList().leftPush(key, value);
    }

    /**
     * 将list放入缓存
     * @param key
     * @param collection
     */
    public void leftPushAll(String key, List<String> collection){
        stringRedisTemplate.opsForList().rightPushAll(key, collection);
    }

    /**
     * list中存入一个元素
     * @param key
     * @param value
     */
    public void rightPush(String key, String value){
        stringRedisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 将list放入缓存
     * @param key
     * @param collection
     */
    public void rightPushAll(String key, List<String> collection){
        stringRedisTemplate.opsForList().rightPushAll(key, collection);
    }

    /**
     * 获取list中的元素，索引从start到end
     * @param key
     * @param start
     * @param end
     * @return
     */
    public List<String> range(String key, long start, long end){
        return stringRedisTemplate.opsForList().range(key, start, end);
    }

    /**
     * 获取list的size
     * @param key
     * @return
     */
    public long lSize(String key){
        return stringRedisTemplate.opsForList().size(key);
    }

    /**
     * 根据index获取list中的元素
     * @param key
     * @param index
     * @return
     */
    public String lIndex(String key, long index){
        return stringRedisTemplate.opsForList().index(key, index);
    }

    /**
     * 指定index修改list中的元素
     * @param key
     * @param index
     * @param value
     */
    public void lSet(String key, long index, String value){
        stringRedisTemplate.opsForList().set(key, index, value);

    }

    /**
     * 移除N个值为value
     * @param key
     * @param count
     * @param value
     * @return 移除的个数
     */
    public long lRemove(String key, long count, String value){
        return stringRedisTemplate.opsForList().remove(key, count, value);
    }
}