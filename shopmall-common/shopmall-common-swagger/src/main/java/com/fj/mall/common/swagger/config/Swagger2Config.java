package com.fj.mall.common.swagger.config;

import com.fj.mall.common.info.constants.CommonConstant;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author: fengjun
 * @Copyright: 合肥
 * @Description:
 * @Date: 2020/8/5 下午 06:01
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket createRestApi(){
        return new Docket(DocumentationType.SWAGGER_2) //指定规范，这里是SWAGGER_2
                .apiInfo(apiInfo()) //设定Api文档头信息，这个信息会展示在文档UI的头部位置
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
//                .securitySchemes(Lists.newArrayList(apiKey()))
                .globalOperationParameters(Lists.newArrayList(auth()));
    }

    private ApiInfo apiInfo(){
        Contact contact = new Contact("Mall商城研发中心", "http://www.fj.com.cn/", "windy_85@126.com");
        return new ApiInfoBuilder()
                .contact(contact)
                .title("Mall商城 API文档")
                .description("Mall商城 API文档")
                .version("1.0.0")
                .build();
    }

    private Parameter auth(){
        return new ParameterBuilder()
                .name("Authorization")
                .description("令牌")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey("userKey", CommonConstant.USERINFO_HEADER, "header");
    }

}
