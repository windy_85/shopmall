package com.fj.mall.goods.api.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author fj
 * @since 2020-07-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("pms_goods")
public class PmsGoods extends Model<PmsGoods> {

    private static final long serialVersionUID=1L;

    /**
     * 商品id
     */
    private Long id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品分类id
     */
    private Integer categoryId;

    /**
     * 商品所属店铺id
     */
    private Long shopId;

    /**
     * 商品所属店铺名称
     */
    private String shopName;

    /**
     * 品牌id
     */
    private Integer brandId;

    /**
     * 商品品牌名称
     */
    private String brandName;

    /**
     * 运费模板id
     */
    private Integer feightTemplateId;

    /**
     * 商品小标题
     */
    private String subTitle;

    /**
     * 商品主图
     */
    private String mainImage;

    /**
     * 商品附图，连主图一起共5张，以逗号分隔
     */
    private String subImages;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 商品库存
     */
    private Integer stock;

    /**
     * 库存预警值
     */
    private Integer stockWarn;

    /**
     * 乐观锁
     */
    private Integer version;

    /**
     * 商品销量
     */
    private Integer sale;


    /**
     * 生产厂家
     */
    private String producer;


    /**
     * 商家编码，商品货号
     */
    private String productNumber;

    /**
     * 运营分类id
     */
    private Integer operationCategoryId;

    /**
     * 商品状态：0->未审核、1->审核未通过，2->已上架，3->已下架
     */
    private Integer status;

    /**
     * 是否删除 0:不删除，1:删除
     */
    private Boolean isDeleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
