package com.fj.mall.goods.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fj.mall.goods.api.pojo.entity.PmsGoods;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author fj
 * @since 2020-07-08
 */
public interface IPmsGoodsService extends IService<PmsGoods> {

}
