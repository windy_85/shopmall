package com.fj.mall.goods.biz;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: fengjun
 * @Copyright: 合肥
 * @Description:
 * @Date: 2020/7/15 下午 02:23
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GoodsTest {
    Logger logger = LoggerFactory.getLogger(GoodsTest.class);

    @Test
    public void testLogger(){

        logger.info("test slfj logger");
    }
}
