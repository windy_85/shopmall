package com.fj.mall.goods.biz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: fengjun
 * @Copyright: 合肥
 * @Description:
 * @Date: 2020/7/7 上午 09:34
 */
@SpringBootApplication
public class GoodsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoodsApplication.class, args);
    }

}
