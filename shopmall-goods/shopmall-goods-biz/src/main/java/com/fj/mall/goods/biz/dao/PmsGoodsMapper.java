package com.fj.mall.goods.biz.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fj.mall.goods.api.pojo.entity.PmsGoods;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author fj
 * @since 2020-07-08
 */
public interface PmsGoodsMapper extends BaseMapper<PmsGoods> {

}
