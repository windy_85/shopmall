package com.fj.mall.goods.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fj.mall.common.info.response.R;
import com.fj.mall.goods.api.pojo.entity.PmsGoods;
import com.fj.mall.goods.api.pojo.param.PmsGoodsParam;
import com.fj.mall.goods.api.service.IPmsGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author fj
 * @since 2020-07-08
 */
@Api(value = "商品服务模块", tags = "商品服务")
@RestController
@RequestMapping("/mall/goods")
public class PmsGoodsController {

    @Autowired
    private IPmsGoodsService goodsService;

    @ApiOperation(value = "添加商品")
    @PostMapping("/create")
    public R create(PmsGoodsParam goodsParam){
        PmsGoods goods = new PmsGoods();
        BeanUtils.copyProperties(goodsParam, goods);
        goodsService.save(goods);
        return R.ok();
    }

    @ApiOperation(value = "查看商品列表")
    @ApiImplicitParams({@ApiImplicitParam(name = "pageNum", value = "当前页数", required = true, paramType = "query", dataType = "integer"),
                        @ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, paramType = "query", dataType = "integer")})
    @GetMapping("/list")
    public R list(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize){
        List<PmsGoods> list = goodsService.list(new QueryWrapper<PmsGoods>());
        return R.ok(list);
    }

}

