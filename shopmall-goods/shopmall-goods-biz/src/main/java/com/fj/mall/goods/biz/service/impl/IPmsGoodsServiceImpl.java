package com.fj.mall.goods.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fj.mall.goods.api.pojo.entity.PmsGoods;
import com.fj.mall.goods.api.service.IPmsGoodsService;
import com.fj.mall.goods.biz.dao.PmsGoodsMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author fj
 * @since 2020-07-08
 */
@Service
public class IPmsGoodsServiceImpl extends ServiceImpl<PmsGoodsMapper, PmsGoods> implements IPmsGoodsService {

}
